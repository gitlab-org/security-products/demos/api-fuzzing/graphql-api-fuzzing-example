@ECHO off
SetLocal

SET FUZZAPI_SDK=%~dp0\..\..
SET CI_EXAMPLES=%FUZZAPI_SDK%\ci\examples
SET GRAPHQL=%~dp0
SET PYTHONPATH=%FUZZAPI_SDK%\tg\pytest\pytest-peach
SET FUZZAPI_AUTOMATION_CMD=python -m pytest --peach=on %GRAPHQL%\tests.py

CALL %CI_EXAMPLES%\run_test.bat

rem end
